import React, { Component } from 'react';
import { Route, Switch, Redirect, withRouter } from 'react-router-dom';
import { renderToStaticMarkup } from 'react-dom/server';
import { withLocalize } from 'react-localize-redux';

import './App.css';

import enTranslation from './assets/translations/en.json';
import viTranslation from './assets/translations/vi.json';

import HomePage from './containers/_pages/Home';
import Login from "./containers/_pages/Home/Login";
import Share from "./containers/_pages/Home/Share";
import Register from './containers/_pages/Home/Register';
import ErrorPage from './containers/_pages/Error';
import { compose } from "redux";
import { logout } from "./actions/auth";
import connect from "react-redux/es/connect/connect";
import { getTokenErrors } from "./selectors/getTokenErr";

class App extends Component {
    constructor(props) {
        super(props);

        this.props.initialize({
            languages: [
                { code: "en" },
                { code: "vi" },
            ],
            options: { renderToStaticMarkup }
        });
        this.props.addTranslationForLanguage(enTranslation, "en");
        this.props.addTranslationForLanguage(viTranslation, "vi");
    }

    handleLogout() {
        this.props.logout();
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.tokenErrors !== this.props.tokenErrors) {
            this.handleLogout();
        }
    }

    render() {
        return (
            <div className="app">
                <Switch>
                    <Route path="/error" component={ErrorPage} />
                    <Route path="/login" component={Login} />
                    <Route path="/register" component={Register} />
                    <Route path="/share-movie" component={Share} />
                    <Route path="/" component={HomePage} />
                    <Redirect to="/error/404" />
                </Switch>
            </div>
        );
    }
}

const mapStateToProps = (store, props) => {
    return {
        tokenErrors: getTokenErrors(store, props),
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        logout: () => {
            dispatch(logout());
        }
    }
};
export default compose(withLocalize, withRouter, connect(mapStateToProps, mapDispatchToProps))(App);

