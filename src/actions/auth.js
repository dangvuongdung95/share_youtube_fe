import {callApi} from "../utils/commonUtils";

const ACTIONS = {
    LOGIN_PROGRESS: "LOGIN_PROGRESS",
    LOGIN_SUCCESS: "LOGIN_SUCCESS",
    LOGIN_FAILED: "LOGIN_FAILED",
    REGISTER_PROGRESS: 'REGISTER_PROGRESS',
    REGISTER_SUCCESS: 'REGISTER_SUCCESS',
    REGISTER_FAILED: 'REGISTER_FAILED',
    LOGOUT: "LOGOUT",
};

const updateStatus = (status, statusInfo) => {
    return {
        type: status,
        data: statusInfo
    }
};

const login = (data) => {
    const config = {
        method: 'post',
        body: data
    };
    return (dispatch) => {
        dispatch(updateStatus(ACTIONS.LOGIN_PROGRESS));
        callApi('/api/login',
            config,
            null,
            (data) => {
                dispatch(updateStatus(ACTIONS.LOGIN_SUCCESS, {user: data}));
            },
            (err) => {
                dispatch(updateStatus(ACTIONS.LOGIN_FAILED, {error: err}));
            }
        );
    };
};

const register = (data) => {
    const config = {
        method: 'post',
        body: data
    };
    return (dispatch) => {
        dispatch(updateStatus(ACTIONS.REGISTER_PROGRESS));
        callApi('/api/register',
            config,
            null,
            (data) => {
                dispatch(updateStatus(ACTIONS.REGISTER_SUCCESS, {user: data}));
            },
            (err) => {
                dispatch(updateStatus(ACTIONS.REGISTER_FAILED, {error: err}));
            }
        );
    };
};

const logout = () => {
    return {
        type: ACTIONS.LOGOUT
    }
};

export {ACTIONS, login, register, logout};
