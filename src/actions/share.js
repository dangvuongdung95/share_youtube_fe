import {callApi} from "../utils/commonUtils";

const ACTIONS = {
    GET_DATA_PROGRESS: 'GET_DATA_PROGRESS',
    GET_DATA_SUCCESS: 'GET_DATA_SUCCESS',
    GET_DATA_FAILED: 'GET_DATA_FAILED',
    SHARE_PROGRESS: 'SHARE_PROGRESS',
    SHARE_SUCCESS: 'SHARE_SUCCESS',
    SHARE_FAILED: 'SHARE_FAILED',
};

const updateStatus = (status, statusInfo) => {
    return {
        type: status,
        data: statusInfo
    }
};

const getData = (data) => {
    let config = {
        method: 'post',
        body: {
            page: data.page,
            limit: 3
        }
    };
    return (dispatch) => {
        dispatch(updateStatus(ACTIONS.GET_DATA_PROGRESS));
        callApi('/api/get_data',
            config,
            null,
            (data) => {
                dispatch(updateStatus(ACTIONS.GET_DATA_SUCCESS, {dataShare: data.docs, pages: data.pages}));
            },
            (err) => {
                dispatch(updateStatus(ACTIONS.GET_DATA_FAILED, {error: err}));
            }
        );
    };
};

const share = (data) => {
    let config = {
        isAuthorization: true,
        method: 'post',
        body: data
    };
    return (dispatch) => {
        dispatch(updateStatus(ACTIONS.SHARE_PROGRESS));
        callApi('/api/share',
            config,
            null,
            () => {
                dispatch(updateStatus(ACTIONS.SHARE_SUCCESS));
            },
            (err) => {
                dispatch(updateStatus(ACTIONS.SHARE_FAILED, {error: err}));
            }
        );
    };
};

export {ACTIONS, getData, share};