import React, { Component } from "react";
import { connect } from "react-redux";
import { logout } from "../../../../actions/auth";
import { withRouter, Link } from 'react-router-dom';
import { Button } from "antd";
import "./style.css";

class Header extends Component {
    render() {
        const email = localStorage.getItem("EMAIL");
        return (
            <div className="component-header d-flex justify-content-between ">
                <Link to='/' className="title">Funny Movies</Link>
                <div className="d-flex justify-content-between align-content-center align-items-baseline right-content">
                    {email ?
                        <React.Fragment>
                            <span className='mr-2 mb-2'>Welcome {email}</span>
                            <Button className='mb-2 mr-2' onClick={() => this.props.history.push('/share-movie')}>Share
                                a movie</Button>
                            <Button className="logout" onClick={() => {
                                this.props.logout();
                                this.props.history.push('/login')
                            }}>Logout</Button>
                        </React.Fragment> :
                        <React.Fragment>
                            <Button className='mr-2 mb-2' style={{width: '100%'}} onClick={() => this.props.history.push('/login')}>Login</Button>
                            <Button onClick={() => this.props.history.push('/register')}>Register</Button>
                        </React.Fragment>
                    }
                </div>
            </div>
        )
    }
}

function mapDispatchToProps(dispatch) {
    return {
        logout: () => {
            dispatch(logout())
        }
    }
}

export default connect(null, mapDispatchToProps)(withRouter(Header));
