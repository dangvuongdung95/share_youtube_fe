import React from "react";
import {connect} from "react-redux";
import {ACTIONS, register} from "../../../../actions/auth";
import {Link} from 'react-router-dom';
import {Form, Icon, Input, Button} from 'antd';
import "./style.css";
import Header from '../../Home/Header';

class Register extends React.Component {
    constructor() {
        super();
        this.state = {}
    }

    handleSubmit = e => {
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
            if (!err) {
                this.props.register(values)
            }
        });
    };

    componentWillReceiveProps(nextProps) {
        if (nextProps.status === ACTIONS.REGISTER_SUCCESS) {
            this.props.history.push('/')
        }
    }

    render() {
        const {getFieldDecorator} = this.props.form;
        return (
            <div className="register-container">
                <Header/>
                <div className="section d-flex container">
                    <div className="content">
                        <Form onSubmit={this.handleSubmit} className="register-form">
                            <Form.Item>
                                {getFieldDecorator('email', {
                                    rules: [
                                        {required: true, message: 'Please input your E-mail!'},
                                        {
                                            type: 'email',
                                            message: 'The input is not valid E-mail!',
                                        }],
                                })(
                                    <Input
                                        prefix={<Icon type="mail" style={{color: 'rgba(0,0,0,.25)'}}/>}
                                        placeholder="Email"
                                    />,
                                )}
                            </Form.Item>
                            <Form.Item>
                                {getFieldDecorator('password', {
                                    rules: [{required: true, message: 'Please input your Password!'}],
                                })(
                                    <Input
                                        prefix={<Icon type="lock" style={{color: 'rgba(0,0,0,.25)'}}/>}
                                        type="password"
                                        placeholder="Password"
                                    />,
                                )}
                            </Form.Item>
                            <Form.Item>
                                <Button type="primary" htmlType="submit"
                                        className="register-form-button">Register</Button>
                                Or <Link to="/login">login now!</Link>
                            </Form.Item>
                        </Form>
                    </div>
                </div>
            </div>
        )
    }
}

const WrappedNormalRegister = Form.create({name: 'normal_register'})(Register);

const mapStateToProps = (state) => {
    return {
        status: state.auth.status,
        err: state.auth.authError,
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        register: (data) => {
            dispatch(register(data))
        }
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(WrappedNormalRegister);

