import React from "react";
import {connect} from "react-redux";
import {Form, Input, Button, message} from 'antd';
import {ACTIONS, share} from "../../../../actions/share";
import {Redirect} from 'react-router-dom';
import Header from '../../Home/Header';
import "./style.css";

class Share extends React.Component {
    constructor() {
        super();
        this.state = {}
    }

    handleSubmit = e => {
        e.preventDefault();
        this.props.form.validateFieldsAndScroll((err, values) => {
            if (!err) {
                // eslint-disable-next-line
                const Regex = /^((?:https?:)?\/\/)?((?:www|m)\.)?((?:youtube\.com|youtu.be))(\/(?:[\w\-]+\?v=|embed\/|v\/)?)([\w\-]+)(\S+)?$/;
                if (values.url.match(Regex)) {
                    this.props.share(values)
                } else {
                    message.error('Youtube URL is wrong! Please input again!')
                }
            }
        });
    };

    componentWillReceiveProps(nextProps) {
        if (nextProps.status === ACTIONS.SHARE_SUCCESS) {
            this.props.form.resetFields()
        }
    }

    render() {
        const checkLogin = localStorage.getItem('EMAIL');
        const {getFieldDecorator} = this.props.form;
        const formItemLayout = {
            labelCol: {
                xs: {span: 24},
                sm: {span: 6},
            },
            wrapperCol: {
                xs: {span: 24},
                sm: {span: 18},
            },
        };
        const tailFormItemLayout = {
            wrapperCol: {
                xs: {
                    span: 24,
                    offset: 0,
                },
                sm: {
                    span: 16,
                    offset: 6,
                },
            },
        };
        return (
            <div className="share-container">
                {!checkLogin && <Redirect to='/'/>}
                <Header/>
                <div className="section d-flex container">
                    <div className="content">
                        <Form {...formItemLayout} onSubmit={this.handleSubmit} className="share-form">
                            <Form.Item label="URL Movie">
                                {getFieldDecorator('url', {
                                    rules: [
                                        {required: true, message: 'Please input your URL!'},
                                    ],
                                })(
                                    <Input
                                        placeholder="URL"
                                    />,
                                )}
                            </Form.Item>
                            <Form.Item {...tailFormItemLayout}>
                                <Button htmlType="submit" className="share-form-button">Share now</Button>
                            </Form.Item>
                        </Form>
                    </div>
                </div>
            </div>
        )
    }
}

const WrappedNormalShareForm = Form.create({name: 'normal_share'})(Share);

const mapStateToProps = (state) => {
    return {
        status: state.share.status,
        err: state.share.shareError,
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        share: (data) => {
            dispatch(share(data))
        }
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(WrappedNormalShareForm);

