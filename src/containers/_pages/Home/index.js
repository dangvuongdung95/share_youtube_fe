import React, { Component } from 'react';
import { connect } from "react-redux";
import Header from "../Home/Header";
import { ACTIONS, getData } from '../../../actions/share'
import ReactPaginate from 'react-paginate';
import { Row, Col } from 'antd';
import ReactPlayer from 'react-player';
import ReadMoreAndLess from 'react-read-more-less';
import "./style.css";

class HomePage extends Component {
    constructor() {
        super();
        this.state = {
            activePage: 0,
            dataShare: []
        }
    }

    componentDidMount() {
        this.props.getData({ page: this.state.activePage });
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.status === ACTIONS.GET_DATA_SUCCESS) {
            this.setState({
                dataShare: nextProps.dataShare
            });
        }
    }

    handlePageClick = data => {
        const page = data.selected + 1;
        this.setState({ activePage: page }, () => {
            document.body.scrollTop = 0; // For Safari
            document.documentElement.scrollTop = 0;
            this.props.getData({
                page: this.state.activePage,
            });
        });
    };

    render() {
        const { dataShare } = this.state;
        return (
            <div className="container-home-page">
                <Header />
                <div className="section d-flex container">
                    <div className="content">
                        {Array.isArray(dataShare) && dataShare.length > 0 &&
                            <React.Fragment>
                                {
                                    dataShare.map((item, index) => {
                                        return (
                                            <Row key={index} gutter={12} className='mb-3'>
                                                <Col lg={12} md={12} sm={24}>
                                                    <ReactPlayer
                                                        controls={true}
                                                        width='100%'
                                                        height='300px'
                                                        url={item.url}
                                                        playing={false}
                                                    />
                                                </Col>

                                                <Col lg={12} md={12} sm={24}>
                                                    <p><strong className='title'>{item.title}</strong></p>
                                                    <p><strong>Share by:</strong> <span style={{color: 'black'}}>{item.userId && item.userId.email}</span></p>
                                                    <strong>Description: </strong>
                                                    <pre style={{ width: '100%' }}>
                                                        <ReadMoreAndLess
                                                            ref={this.ReadMore}
                                                            charLimit={250}
                                                            readMoreText="Read more"
                                                            readLessText="Read less"
                                                        >
                                                            {item.description}
                                                        </ReadMoreAndLess>
                                                    </pre>
                                                    {/* <pre style={{ wordBreak: 'break-word' }}>{item.description}</pre> */}
                                                </Col>
                                            </Row>
                                        )
                                    })
                                }
                                <ReactPaginate
                                    previousLabel={"<"}
                                    nextLabel={">"}
                                    breakLabel={<a href="">...</a>}
                                    breakClassName={"break-me"}
                                    pageCount={this.props.pages}
                                    marginPagesDisplayed={1}
                                    pageRangeDisplayed={2}
                                    onPageChange={this.handlePageClick}
                                    containerClassName={"pagination"}
                                    subContainerClassName={"pages pagination"}
                                    activeClassName={"active"}
                                />
                                <div className='mb-2'>
                                    Link GitLab Project: <a rel="noopener noreferrer" target="_blank" href='https://gitlab.com/dangvuongdung95/share-youtube-be'>BACK-END</a>  <a target="_blank" rel="noopener noreferrer" href='https://gitlab.com/dangvuongdung95/share-youtube-fe'>FRONT-END</a>
                                </div>
                                <div>
                                    Link API DOC: <a rel="noopener noreferrer" target="_blank" href='http://3.14.88.209:3000/'>API-DOC</a>
                                </div>
                            </React.Fragment>
                        }
                    </div>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (store) => {
    return {
        status: store.share.status,
        dataShare: store.share.dataShare,
        pages: store.share.pages,
        err: store.share.authError
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        getData: (data) => {
            dispatch(getData(data))
        }
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(HomePage);
