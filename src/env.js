const ENV = {
    LCL: {
        API_SERVER: 'http://localhost:1995'
    },

    DEV: {
        API_SERVER: 'http://3.14.88.209:1995'
    },

    STG: {
        API_SERVER: ''
    },

    PRO: {
        API_SERVER: ''
    }
};

const config = ENV[process.env.REACT_APP_STAGE || "LCL"];

const getEnv = (name, defaultValue) => {
    return process.env[name] || config[name] || defaultValue;
};

export {getEnv};
