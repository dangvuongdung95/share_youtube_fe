import React from 'react';
import ReactDOM from 'react-dom';
import {Provider} from 'react-redux';
import {BrowserRouter} from 'react-router-dom';
import {LocalizeProvider} from 'react-localize-redux';

import 'bootstrap/dist/css/bootstrap.min.css';
import 'antd/dist/antd.css';
import './utils/commonStyles.css';
import './index.css';

import App from './App';
import configStore from './store';

let store = configStore();

ReactDOM.render(
	<LocalizeProvider>
		<Provider store={store}>
			<BrowserRouter>
				<App/>
			</BrowserRouter>
		</Provider>
	</LocalizeProvider>,
	document.getElementById('root')
);

