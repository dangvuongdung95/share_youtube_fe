import { ACTIONS } from '../actions/auth';
import { message } from 'antd';

const initialState = {
    status: null,
    user: localStorage.getItem("ACCESS_TOKEN"),
    authError: null,
};

export default (state = initialState, action) => {
    switch (action.type) {
        case ACTIONS.LOGIN_PROGRESS:
        case ACTIONS.REGISTER_PROGRESS:
            return {
                ...state,
                status: action.type
            };

        case ACTIONS.LOGIN_SUCCESS:
        case ACTIONS.REGISTER_SUCCESS:
            const user = action.data.user;
            localStorage.setItem("ACCESS_TOKEN", user.token);
            localStorage.setItem("EMAIL", user.email);
            return {
                ...state,
                status: action.type,
                user: { token: user.token }
            };
        case ACTIONS.LOGIN_FAILED:
        case ACTIONS.REGISTER_FAILED:
            action.data.error.mess && message.error(action.data.error.mess)
            return {
                ...state,
                status: action.type,
                authError: action.data.error
            };
        case ACTIONS.LOGOUT:
            localStorage.removeItem("ACCESS_TOKEN");
            localStorage.removeItem("EMAIL");
            return {
                ...state,
                status: action.type,
                user: null
            };
        default:
            return state;
    }
}
