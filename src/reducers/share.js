import { ACTIONS } from '../actions/share';
import { message } from 'antd';

const initialState = {
    status: null,
    dataShare: [],
    pages: null,
    shareError: null,
};
export default (state = initialState, action) => {
    switch (action.type) {
        case ACTIONS.SHARE_PROGRESS:
        case ACTIONS.GET_DATA_PROGRESS:
            return {
                ...state,
                status: action.type
            };
        case ACTIONS.SHARE_SUCCESS:
            message.success('Share movie successfully!')
            return {
                ...state,
                status: action.type
            };
        case ACTIONS.GET_DATA_SUCCESS:
            return {
                ...state,
                status: action.type,
                dataShare: action.data.dataShare,
                pages: action.data.pages
            };
        case ACTIONS.GET_DATA_FAILED:
        case ACTIONS.SHARE_FAILED:
            action.data.error.mess && message.error(action.data.error.mess)
            return {
                ...state,
                status: action.type,
                shareError: action.data.error
            };
        default:
            return state;
    }
}


