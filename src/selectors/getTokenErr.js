import {createSelector} from 'reselect'
const getErrorAuth = state => state.auth.authError;
const getErrorShare = state => state.share.shareError;

const getTokenErrors = createSelector(
    [
        getErrorAuth,
        getErrorShare,
    ],
    (
        authErr,
        shareError,
    ) => {
        const errorsList = [
            authErr,
            shareError,
        ];
        return errorsList.find(error => error && error.code === 'NOT_AUTHENTICATED_ERROR');
    }
);
export {
    getTokenErrors,
}
