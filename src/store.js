import {createStore, applyMiddleware, combineReducers} from 'redux';
import thunkMiddleware from 'redux-thunk';
import auth from './reducers/auth';
import share from './reducers/share';

const initialState = {};

export default () => {
    return createStore(
        combineReducers({
            auth,
            share,
        }),
        initialState,
        applyMiddleware(thunkMiddleware)
    );
}
