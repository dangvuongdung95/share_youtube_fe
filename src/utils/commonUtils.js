import 'isomorphic-fetch';
import { getEnv } from '../env';
import { message } from 'antd';

const constructRequest = (config) => {
    if (config && config.isAuthorization)
        config = {
            ...config,
            headers: {
                ...config.headers,
                "Authorization": getToken()
            }
        };
    if (config && config.isFormData) {
        config = {
            ...config,
            body: config.body
        };
    } else {
        if (config.method && config.method.toLowerCase() === 'post')
            config = {
                ...config,
                headers: {
                    ...config.headers,
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify(config.body)
            };
    }
    return config;
};

const callApi = (url,
    config,
    onRequestFinish,
    onRequestSuccess,
    onRequestFailure) => {
    return fetch(getEnv('API_SERVER') + url, constructRequest(config))
        .then(response => {
            if (config.isFileDownload) {
                onRequestSuccess && onRequestSuccess(response);
            } else {
                return response.json();
            }
        })
        .then((data) => {
            onRequestFinish && onRequestFinish();
            data && data.success && !config.isFileDownload && onRequestSuccess && onRequestSuccess(data.result);
            (!data || (data && !data.success)) && onRequestFailure && onRequestFailure(data ? data.error : { code: 'UNKNOWN_ERROR' });
        })
        .catch((err) => {
            console.log(err);
            message.error('Something is wrong in server. Please comeback later!!!');
            onRequestFinish && onRequestFinish();
            onRequestFailure && onRequestFailure({ code: 'NETWORK_ERROR' });
        });
};

const getToken = () => {
    return localStorage.getItem("ACCESS_TOKEN");
};

export { callApi, getToken };
